var searchData=
[
  ['n_5fratios',['N_RATIOS',['../Calibration_8cpp.html#a2179570821d977ce62be319d8e6a1124',1,'Calibration.cpp']]],
  ['networktab',['NetworkTab',['../classhaytham_1_1NetworkTab.html',1,'haytham']]],
  ['networktab',['NetworkTab',['../classhaytham_1_1NetworkTab.html#a2ac45a0c58a7128ca556ac42fc004989',1,'haytham::NetworkTab']]],
  ['networktab_2ecpp',['NetworkTab.cpp',['../NetworkTab_8cpp.html',1,'']]],
  ['networktab_2eh',['NetworkTab.h',['../NetworkTab_8h.html',1,'']]],
  ['neutral',['NEUTRAL',['../structhaytham_1_1HeadGesture.html#ade9e7a06ab3932ea79709881b197716a',1,'haytham::HeadGesture']]],
  ['newcontroller',['newController',['../classhaytham_1_1CameraController.html#a61db0e5bcf1b50a3c0c41b2bffc43b01',1,'haytham::CameraController']]],
  ['newframe',['newFrame',['../classhaytham_1_1VideoProcessingThread.html#a3f6892317d3b620372cac4e90bcff6ab',1,'haytham::VideoProcessingThread']]],
  ['next',['next',['../classhaytham_1_1CircularBuffer_1_1CircularBufferIterator.html#a7a025a4ce70aae9e431f0478fdfc6b18',1,'haytham::CircularBuffer::CircularBufferIterator']]],
  ['norm',['norm',['../classhaytham_1_1Point.html#a97e1b1b13f1ec352ee1004cee06530ac',1,'haytham::Point']]],
  ['now',['now',['../classhaytham_1_1Timer.html#aa005c539cb19bb0897b9f64e2d4a4d7c',1,'haytham::Timer']]]
];
