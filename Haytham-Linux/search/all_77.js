var searchData=
[
  ['what',['what',['../classhaytham_1_1IndexOutOfBoundsException.html#a72ae1a6a7f763b112dc282665dfb76c7',1,'haytham::IndexOutOfBoundsException::what()'],['../classhaytham_1_1MethodNotImplementedException.html#a1dfa10fec60d0707829ef8ff7d5e91b0',1,'haytham::MethodNotImplementedException::what()']]],
  ['whitebalance',['whiteBalance',['../structuvc__controls__t.html#ad47fbb3d86805882b0ed9916442d3391',1,'uvc_controls_t']]],
  ['windowscameracontroller',['WindowsCameraController',['../classhaytham_1_1WindowsCameraController.html',1,'haytham']]],
  ['write',['write',['../classhaytham_1_1DataRecorder.html#a51a5122647d00e54b9df87abe5e8f785',1,'haytham::DataRecorder::write(QString &amp;text)'],['../classhaytham_1_1DataRecorder.html#a826fff891e2630eb2a696935ecc1bcef',1,'haytham::DataRecorder::write(const std::string &amp;text)'],['../classhaytham_1_1GazeDataCsvRecorder.html#a2052243a4ae0b386bdaccf18cedf2b90',1,'haytham::GazeDataCsvRecorder::write(GazeData &amp;data)'],['../classhaytham_1_1GazeDataCsvRecorder.html#ad1fc603c882147f2c06327f404818c41',1,'haytham::GazeDataCsvRecorder::write(HeadGesture &amp;gesture)']]]
];
