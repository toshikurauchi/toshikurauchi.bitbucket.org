var searchData=
[
  ['uvc_5fcontrol_5finterface_5fclass',['UVC_CONTROL_INTERFACE_CLASS',['../UVCCameraControl_8h.html#a4697c44c3bb3de9ab920ab1a7583b2b0',1,'UVCCameraControl.h']]],
  ['uvc_5fcontrol_5finterface_5fsubclass',['UVC_CONTROL_INTERFACE_SUBCLASS',['../UVCCameraControl_8h.html#a1e4bed373c8b84ea6ca28ad9ae075eee',1,'UVCCameraControl.h']]],
  ['uvc_5fget_5fcur',['UVC_GET_CUR',['../UVCCameraControl_8h.html#aa04d5ed4f063f9177295fbbde38aaac3',1,'UVCCameraControl.h']]],
  ['uvc_5fget_5fmax',['UVC_GET_MAX',['../UVCCameraControl_8h.html#a316711167773bda5582ebb85bc388d94',1,'UVCCameraControl.h']]],
  ['uvc_5fget_5fmin',['UVC_GET_MIN',['../UVCCameraControl_8h.html#a3f4becff4891e3283596ca80fa640ce3',1,'UVCCameraControl.h']]],
  ['uvc_5finput_5fterminal_5fid',['UVC_INPUT_TERMINAL_ID',['../UVCCameraControl_8h.html#a86f205900db12eb0c9639bf28f424b4e',1,'UVCCameraControl.h']]],
  ['uvc_5fprocessing_5funit_5fid',['UVC_PROCESSING_UNIT_ID',['../UVCCameraControl_8h.html#ad078b837fb56b489dd6b4d96ce182663',1,'UVCCameraControl.h']]],
  ['uvc_5fset_5fcur',['UVC_SET_CUR',['../UVCCameraControl_8h.html#a4cb7204bbf19dc57e2e20bf9a9b52455',1,'UVCCameraControl.h']]]
];
