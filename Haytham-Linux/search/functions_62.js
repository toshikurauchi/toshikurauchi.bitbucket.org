var searchData=
[
  ['bgr2gray',['bgr2gray',['../classhaytham_1_1ImageProcessing.html#a2925fec1d8627b818d756d532a31631b',1,'haytham::ImageProcessing']]],
  ['blink',['blink',['../classhaytham_1_1ThresholdEyeDetector.html#a6e5c0c80b285b9bcd5c014be10ec7724',1,'haytham::ThresholdEyeDetector']]],
  ['blob',['Blob',['../classhaytham_1_1Blob.html#ae6a8fe350222057f1ee828c59b235e1b',1,'haytham::Blob::Blob()'],['../classhaytham_1_1Blob.html#ac0896f143414167bb5b13fb3d577b138',1,'haytham::Blob::Blob(std::vector&lt; cv::Point &gt; blobPoints)']]],
  ['boost_5fpython_5fmodule',['BOOST_PYTHON_MODULE',['../hello_8cpp.html#a5c1dc20f51dbed79d85a0681683fc0c0',1,'hello.cpp']]],
  ['borderintersectsroiborder',['borderIntersectsROIBorder',['../classhaytham_1_1EyeBlob.html#a04749cccde6e745be14a8b176b1f2861',1,'haytham::EyeBlob']]],
  ['byarea',['byArea',['../namespacehaytham.html#a08a3b99e57bb7d29ef88775d95e0875e',1,'haytham']]]
];
