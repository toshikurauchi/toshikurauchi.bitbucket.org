var searchData=
[
  ['calcminarea',['calcMinArea',['../classhaytham_1_1MonitorDetector.html#a46c21de29d23bae9efc4ffad5752ceb5',1,'haytham::MonitorDetector']]],
  ['calcroi',['calcROI',['../classhaytham_1_1ImageProcessing.html#a518f9d9921f4134fdb62b73d52bf9f51',1,'haytham::ImageProcessing']]],
  ['calculateeyefeatures',['calculateEyeFeatures',['../classhaytham_1_1PolynomialCalibration.html#a3e85d8122a794c970ba1a433978da864',1,'haytham::PolynomialCalibration::calculateEyeFeatures()'],['../classhaytham_1_1PolynomialCalibration9Points.html#a32e0f05ba5cb51f81846c91566050645',1,'haytham::PolynomialCalibration9Points::calculateEyeFeatures()'],['../classhaytham_1_1PolynomialCalibration4Points.html#a386ce842d82739a5d9a11d12d209378b',1,'haytham::PolynomialCalibration4Points::calculateEyeFeatures()']]],
  ['calculatehull',['calculateHull',['../classhaytham_1_1Blob.html#a3469147d04357a61cffdc6c29082142e',1,'haytham::Blob']]],
  ['calibrate',['calibrate',['../classhaytham_1_1Calibration.html#a03016804caea95687a582b41d6d68cfc',1,'haytham::Calibration']]],
  ['calibratecamera',['calibrateCamera',['../classhaytham_1_1SceneTab.html#ab1848508eb9764e8eaa87d7a91850863',1,'haytham::SceneTab']]],
  ['calibration',['Calibration',['../classhaytham_1_1Calibration.html#a6408297ad69b7fa992c222afbfb24e4b',1,'haytham::Calibration']]],
  ['calibrationfile',['CalibrationFile',['../classhaytham_1_1CalibrationFile.html#a2a92c03ca0d0fff6094115e4a16ff500',1,'haytham::CalibrationFile::CalibrationFile()'],['../classhaytham_1_1CalibrationFile.html#a9d508cfb220709c384fd5f1cc11788f7',1,'haytham::CalibrationFile::CalibrationFile(std::string filename)']]],
  ['calibrationpointsdetection',['CalibrationPointsDetection',['../classhaytham_1_1CalibrationPointsDetection.html#a77a26fdeec20943b1a5f045cfd4ea073',1,'haytham::CalibrationPointsDetection']]],
  ['calibrationpointslabel',['CalibrationPointsLabel',['../classhaytham_1_1CalibrationPointsLabel.html#a783c6263777223212805b9f437765c9c',1,'haytham::CalibrationPointsLabel']]],
  ['calibrationtab',['CalibrationTab',['../classhaytham_1_1CalibrationTab.html#a218a9dad7721c93609865d95f50c817c',1,'haytham::CalibrationTab']]],
  ['cameracalibration',['CameraCalibration',['../classhaytham_1_1CameraCalibration.html#aa496a6290bb375bda8c0e36019db7394',1,'haytham::CameraCalibration']]],
  ['cameracomponent',['CameraComponent',['../classhaytham_1_1CameraComponent.html#a70a2223d24cf0f093f042270cad17e62',1,'haytham::CameraComponent']]],
  ['camerainfo',['CameraInfo',['../classhaytham_1_1CameraInfo.html#ae42244b35c64081c734bbd812c7c1491',1,'haytham::CameraInfo::CameraInfo()'],['../classhaytham_1_1CameraInfo.html#a6d4213522c1076eca23f580452ea1124',1,'haytham::CameraInfo::CameraInfo(int id, std::string name)']]],
  ['camerasettingsdialog',['CameraSettingsDialog',['../classCameraSettingsDialog.html#ae5c0676c20f8d3a9b23622d8d3f680bc',1,'CameraSettingsDialog']]],
  ['cameratab',['CameraTab',['../classhaytham_1_1CameraTab.html#ae81a419c40a535cbd73839e463d9d4d8',1,'haytham::CameraTab']]],
  ['camerawidget',['CameraWidget',['../classhaytham_1_1CameraWidget.html#a97edad6476c635557b4731449054990a',1,'haytham::CameraWidget']]],
  ['capturethread',['CaptureThread',['../classCaptureThread.html#a88cf72cecf5c6bd4c0f7e4c590c95d0f',1,'CaptureThread']]],
  ['circularbuffer',['CircularBuffer',['../classhaytham_1_1CircularBuffer.html#ad4e1548122146897e654e05e07a15946',1,'haytham::CircularBuffer']]],
  ['circularbufferiterator',['CircularBufferIterator',['../classhaytham_1_1CircularBuffer_1_1CircularBufferIterator.html#acf3a66a50570c5b80a5646089a447f38',1,'haytham::CircularBuffer::CircularBufferIterator']]],
  ['clearbuffer',['clearBuffer',['../classImageBuffer.html#a45e620feb527f0b00483ab9d6fbfd983',1,'ImageBuffer']]],
  ['computeweights',['computeWeights',['../classhaytham_1_1SceneVisualizationGaussian.html#a622a06008156277935b985ef4171b6b5',1,'haytham::SceneVisualizationGaussian::computeWeights()'],['../classhaytham_1_1SceneVisualizationGaussianCumulative.html#afddfba5e7eddfb306f699587c4785028',1,'haytham::SceneVisualizationGaussianCumulative::computeWeights()']]],
  ['compy',['compY',['../namespacehaytham.html#a3a7d7ca0add3a45196e6e2565e1d2e4b',1,'haytham']]],
  ['connectordisconnectcamera',['connectOrDisconnectCamera',['../classhaytham_1_1CameraTab.html#a706df261134738e9dfbd1c240d560ac3',1,'haytham::CameraTab']]],
  ['connecttocamera',['connectToCamera',['../classCaptureThread.html#a9b5e528cc131c8af10673ac0aea3e92d',1,'CaptureThread::connectToCamera()'],['../classController.html#a6db6b8713941e73e1eba87f9a8b66c99',1,'Controller::connectToCamera()'],['../classhaytham_1_1CameraWidget.html#a6e356d78d286da17da1847d86d10cdce',1,'haytham::CameraWidget::connectToCamera()']]],
  ['contourutil',['ContourUtil',['../classhaytham_1_1ContourUtil.html#a5bb98965c2e9197382625fa0768b9faa',1,'haytham::ContourUtil']]],
  ['contrastmonitordetector',['ContrastMonitorDetector',['../classhaytham_1_1ContrastMonitorDetector.html#a737c05fe2adb33080effbbb73386b980',1,'haytham::ContrastMonitorDetector']]],
  ['controller',['Controller',['../classController.html#a95c56822d667e94b031451729ce069a9',1,'Controller']]],
  ['createroiforimage',['createROIForImage',['../classhaytham_1_1ImageProcessing.html#aef56d27f36e27e5d3dddfd47988b768c',1,'haytham::ImageProcessing']]],
  ['createserver',['createServer',['../classhaytham_1_1METState.html#aeb66fda27b159d18b8f42cf327a7190a',1,'haytham::METState']]],
  ['cumsum',['cumsum',['../classhaytham_1_1MatAlgebra.html#adc1df04705402e030398551fe25239ca',1,'haytham::MatAlgebra']]]
];
