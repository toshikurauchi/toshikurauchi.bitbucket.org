var searchData=
[
  ['datamanager',['DataManager',['../classhaytham_1_1DataManager.html#afd69d707363ffc648fcc182aa86e1a50',1,'haytham::DataManager']]],
  ['datarecorder',['DataRecorder',['../classhaytham_1_1DataRecorder.html#acb3a77e45808b87c84a4d6318ae26e30',1,'haytham::DataRecorder']]],
  ['datatab',['DataTab',['../classhaytham_1_1DataTab.html#aee0ee0330ebb920d246b2a6a0d9c0bb4',1,'haytham::DataTab']]],
  ['decarea',['decArea',['../namespacehaytham.html#aa02c3bb819468e8b1c45376262d23f81',1,'haytham']]],
  ['detect',['detect',['../classhaytham_1_1HeadGestureProcessor.html#ad5b0ff3c2cfb6de92f128efb0c66d826',1,'haytham::HeadGestureProcessor']]],
  ['detectglint',['detectGlint',['../classhaytham_1_1GlintDetector.html#a815ca7f315e3ab536e66a7715be3509f',1,'haytham::GlintDetector']]],
  ['detectpupilblob',['detectPupilBlob',['../classhaytham_1_1ThresholdEyeDetector.html#a4f1744bb3af9de706a94c336a5a88038',1,'haytham::ThresholdEyeDetector']]],
  ['detectscreen',['detectScreen',['../classhaytham_1_1MonitorDetector.html#ae4a2f78d968e910dc22fadfc81a2ed2f',1,'haytham::MonitorDetector::detectScreen()'],['../classhaytham_1_1ContrastMonitorDetector.html#a27816dc29115a5494cea4551708a662c',1,'haytham::ContrastMonitorDetector::detectScreen()'],['../classhaytham_1_1LedMonitorDetector.html#afcb758570c0a59618042f985e995158d',1,'haytham::LedMonitorDetector::detectScreen()']]],
  ['disconnectcamera',['disconnectCamera',['../classCaptureThread.html#aabadb09c202ea64608a24e6331cab47c',1,'CaptureThread::disconnectCamera()'],['../classController.html#a2a0f45fb33ad05915a967c1553cb45a1',1,'Controller::disconnectCamera()'],['../classhaytham_1_1CameraWidget.html#a27ad260565396af1d7965abffc7a6379',1,'haytham::CameraWidget::disconnectCamera()']]],
  ['draw',['draw',['../namespacehaytham.html#a998ccda04195042123a340960f7295e1',1,'haytham']]],
  ['drawcross',['drawCross',['../classhaytham_1_1ImageProcessing.html#a469c42a6f7d3b5d5294fbc0f6f8c90d8',1,'haytham::ImageProcessing']]],
  ['drawellipse',['drawEllipse',['../classhaytham_1_1ImageProcessing.html#aaa63decbb96e1c68018cd5042b490a29',1,'haytham::ImageProcessing']]],
  ['drawiriscircle',['drawIrisCircle',['../classhaytham_1_1ImageProcessing.html#ae817ecb6f36fb28456c34aabd45f8a8f',1,'haytham::ImageProcessing']]],
  ['drawline',['drawLine',['../classhaytham_1_1ImageProcessing.html#afeac7ca02b729d05a09b4cd81012fc7a',1,'haytham::ImageProcessing']]],
  ['drawpoint',['drawPoint',['../classhaytham_1_1ImageProcessing.html#addc595462133a4d09851013835e2535e',1,'haytham::ImageProcessing']]],
  ['drawrectangle',['drawRectangle',['../classhaytham_1_1ImageProcessing.html#a3beef9696384e9d0d5ee2bd01416883e',1,'haytham::ImageProcessing::drawRectangle(cv::Mat &amp;img, Point center, cv::Size2f size)'],['../classhaytham_1_1ImageProcessing.html#a1ea99d59332d235bd71be2411f69e88a',1,'haytham::ImageProcessing::drawRectangle(cv::Mat &amp;img, Point corners[4], int expand, bool fill, std::string text)']]],
  ['drawscreen',['drawScreen',['../classhaytham_1_1MonitorDetector.html#af18f35242b245e626f552692c33f37f9',1,'haytham::MonitorDetector']]]
];
