var searchData=
[
  ['handlegazedata',['handleGazeData',['../classhaytham_1_1DataManager.html#a8828bcad8ddbf6783bfda122219076ab',1,'haytham::DataManager']]],
  ['handleheadgesture',['handleHeadGesture',['../classhaytham_1_1DataManager.html#a8a7585ce521fd7ae77c5c7b0bea07460',1,'haytham::DataManager']]],
  ['hasnext',['hasNext',['../classhaytham_1_1CircularBuffer_1_1CircularBufferIterator.html#ab200fd3d835693426277e6b9cbb25398',1,'haytham::CircularBuffer::CircularBufferIterator']]],
  ['headgesture',['HeadGesture',['../structhaytham_1_1HeadGesture.html#ada9ce97622811c75d7fbd00c0a911ee7',1,'haytham::HeadGesture']]],
  ['headgestureprocessor',['HeadGestureProcessor',['../classhaytham_1_1HeadGestureProcessor.html#a8d4dfc8d5189138721e804b001f62229',1,'haytham::HeadGestureProcessor']]],
  ['homographycalibration',['HomographyCalibration',['../classhaytham_1_1HomographyCalibration.html#aafa534c585fcfbf875853d4e51984f7c',1,'haytham::HomographyCalibration']]]
];
