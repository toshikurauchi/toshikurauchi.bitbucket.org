var searchData=
[
  ['main',['main',['../main_8cpp.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main.cpp']]],
  ['mainfunction',['mainFunction',['../classhaytham_1_1SceneMain.html#ab641d245ba14484035c6e230880129e1',1,'haytham::SceneMain']]],
  ['mainloop',['mainloop',['../classhaytham_1_1vrpn__GazeTracker.html#a25d011ca3e435bfff3969dddc8e33dac',1,'haytham::vrpn_GazeTracker']]],
  ['mainwindow',['MainWindow',['../classMainWindow.html#a8b244be8b7b7db1b08de2a2acb9409db',1,'MainWindow']]],
  ['map',['map',['../classhaytham_1_1Calibration.html#aec5a6a0bb6a87330831c7cd18f8a91fa',1,'haytham::Calibration::map(Point point, SceneData &amp;sceneData, Point correction=Point())'],['../classhaytham_1_1Calibration.html#a5e50e0cc879096b5c8f02ddaea5fad05',1,'haytham::Calibration::map(Point point, Point correction=Point())'],['../classhaytham_1_1Monitor.html#aaea394cd2181ffa61d8ae71a00dbfdba',1,'haytham::Monitor::map()']]],
  ['mapvalue_3afrommin_3amax_3atomin_3amax_3a',['mapValue:fromMin:max:toMin:max:',['../interfaceUVCCameraControl.html#a28a944da1a0335a5e3dc57c3490a980d',1,'UVCCameraControl']]],
  ['matalgebra',['MatAlgebra',['../classhaytham_1_1MatAlgebra.html#ad39ebeca58cebbfe092b39fa45f1181d',1,'haytham::MatAlgebra']]],
  ['mattoqimage',['MatToQImage',['../MatToQImage_8cpp.html#a92e940a88b8fe6f9cee61392c4bdc1d3',1,'MatToQImage(const Mat &amp;mat):&#160;MatToQImage.cpp'],['../MatToQImage_8h.html#aa6830a11ae624b9745a1750c8bcacbbb',1,'MatToQImage(const Mat &amp;):&#160;MatToQImage.cpp']]],
  ['measurepupilcenter',['measurePupilCenter',['../classhaytham_1_1ThresholdEyeDetector.html#a0b7cd56a60195d89ca6ab63ed3eea0c2',1,'haytham::ThresholdEyeDetector']]],
  ['methodchanged',['methodChanged',['../classhaytham_1_1CalibrationTab.html#a5309ab4190adfc54391e642f84de287c',1,'haytham::CalibrationTab']]],
  ['methodnotimplementedexception',['MethodNotImplementedException',['../classhaytham_1_1MethodNotImplementedException.html#a32f232b575dface31677bbfb935d9ff2',1,'haytham::MethodNotImplementedException']]],
  ['monitor',['Monitor',['../classhaytham_1_1Monitor.html#a9e0e6123ee9ed096c917587a005f1427',1,'haytham::Monitor::Monitor()'],['../classhaytham_1_1Monitor.html#aa9af7d5c5a03227846650e20b3e12ec6',1,'haytham::Monitor::Monitor(int width, int height)']]],
  ['monitordetector',['MonitorDetector',['../classhaytham_1_1MonitorDetector.html#aa70be58455e688d70b90ba4c03a95560',1,'haytham::MonitorDetector::MonitorDetector()'],['../classhaytham_1_1MonitorDetector.html#a753145a7edbc75c506f2da15e1ca2b0f',1,'haytham::MonitorDetector::MonitorDetector(std::string methodName)']]],
  ['mousepressevent',['mousePressEvent',['../classhaytham_1_1ImageLabel.html#a017bff0d52425e96087ac3b275459d7b',1,'haytham::ImageLabel']]]
];
