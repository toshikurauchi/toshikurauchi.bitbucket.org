var searchData=
[
  ['darkest_5fregion_5fimage_5fwidth',['DARKEST_REGION_IMAGE_WIDTH',['../namespacehaytham.html#aa5d5ee208adb5ec548b3a86f0376f1bc',1,'haytham']]],
  ['darkest_5fregion_5fmavg_5fn',['DARKEST_REGION_MAVG_N',['../namespacehaytham.html#ae470cd3fc9fe865eae9a00e77da1a30f',1,'haytham']]],
  ['darkest_5fregion_5fpupil_5firis_5fratio',['DARKEST_REGION_PUPIL_IRIS_RATIO',['../namespacehaytham.html#a89079fb6b6100446c085ed6a9bf36ff2',1,'haytham']]],
  ['darkest_5fregion_5fthreshold_5fvicinity',['DARKEST_REGION_THRESHOLD_VICINITY',['../namespacehaytham.html#a2723b82788e88a0b78f803ac439bf12b',1,'haytham']]],
  ['databuffer',['dataBuffer',['../interfaceUVCCameraControl.html#a2b450e5d487c47241b19be54a53fb403',1,'UVCCameraControl']]],
  ['down',['DOWN',['../structhaytham_1_1HeadGesture.html#aa3725bbac6c5a62fd4e007515857d44b',1,'haytham::HeadGesture']]]
];
