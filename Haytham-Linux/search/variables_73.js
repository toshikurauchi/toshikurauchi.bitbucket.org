var searchData=
[
  ['saturation',['saturation',['../structuvc__controls__t.html#ac2cceffec6f3c47b965bc0a34c64ac9d',1,'uvc_controls_t']]],
  ['scene_5fthreshold',['SCENE_THRESHOLD',['../namespacehaytham.html#a1d9ac50d4d67e4f76cd99e60675226eb',1,'haytham']]],
  ['scenegazetracker',['sceneGazeTracker',['../classhaytham_1_1GazeSharer.html#af74a649002714f131e882622d477f55b',1,'haytham::GazeSharer']]],
  ['screen_5fmin_5fsize',['SCREEN_MIN_SIZE',['../namespacehaytham.html#a19b077d396bce7c87531096f5ea118e6',1,'haytham']]],
  ['screencorners',['screenCorners',['../classhaytham_1_1MonitorDetector.html#a2ee676d04a7278e2374192c87e973ede',1,'haytham::MonitorDetector']]],
  ['selector',['selector',['../structuvc__control__info__t.html#a151f8a3b66837c62e8b6b5abe8874348',1,'uvc_control_info_t']]],
  ['sharpness',['sharpness',['../structuvc__controls__t.html#adadffbadf963a11c9a5321e87ac2b766',1,'uvc_controls_t']]],
  ['show_5ffiltered_5fscene_5fimage',['SHOW_FILTERED_SCENE_IMAGE',['../namespacehaytham.html#ae42e05eb140ec0618276be9cd16f64f4',1,'haytham']]],
  ['show_5fgaze',['SHOW_GAZE',['../namespacehaytham.html#ace6b76739d295a2d4c83ce64499e69be',1,'haytham']]],
  ['show_5fglint',['SHOW_GLINT',['../namespacehaytham.html#a4b1ceaa34cbca31254543d42f0025010',1,'haytham']]],
  ['show_5fpupil',['SHOW_PUPIL',['../namespacehaytham.html#a367c75c6c978a65b94284ff7555e9a6d',1,'haytham']]],
  ['show_5fscreen',['SHOW_SCREEN',['../namespacehaytham.html#ad3b23d3e598ab186d3539aa8af57368f',1,'haytham']]],
  ['size',['size',['../structuvc__control__info__t.html#a03b6b5e4e1ce9073e338a71e4c1e30de',1,'uvc_control_info_t']]],
  ['smooth_5fgaze',['SMOOTH_GAZE',['../namespacehaytham.html#a7be93d04425392fb99258585caa7bae4',1,'haytham']]],
  ['state',['state',['../classhaytham_1_1VideoProcessingThread.html#a71310878f70d6ee487da76d782bc4f99',1,'haytham::VideoProcessingThread']]]
];
