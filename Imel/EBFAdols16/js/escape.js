$('.carousel').carousel({
    pause: false, 
    interval: false
});
$('.carousel').click(function() {
    $(this).carousel('next');
});
function aberto(event) {
    if ($("#tranca").hasClass("tranca-aberta")) {
        $("#cadeado-frente").addClass("cadeado-aberto");
    }
}
$("#tranca").bind('webkitTransitionEnd', aberto); // Webkit
$("#tranca").bind('transitionend', aberto); // Mozilla and IE
$("#tranca").bind('oTransitionEnd', aberto); // Opera
function fechado(event) {
    if (!$("#cadeado-frente").hasClass("cadeado-aberto")) {
        $("#tranca").removeClass("tranca-aberta");
    }
}
$("#cadeado-frente").bind('webkitTransitionEnd', fechado); // Webkit
$("#cadeado-frente").bind('transitionend', fechado); // Mozilla and IE
$("#cadeado-frente").bind('oTransitionEnd', fechado); // Opera

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
function checkConditions() {
    var a = $("#n1>.carousel-inner>.active>h1").text();
    var b = $("#n2>.carousel-inner>.active>h1").text();
    var c = $("#n3>.carousel-inner>.active>h1").text();
    distrito = getUrlParameter("distrito");
    cs = {"1": "485", "2": "305", "3": "134", "4": "286", "5": "490", "6": "334"};
    if (a+b+c === cs[distrito]) {
        $("#tranca").addClass("tranca-aberta");
        $("#abrir>h3").text("Fechar");
    }
    else {
        $("#cadeado-frente").removeClass("cadeado-aberto");
        $("#abrir>h3").text("Abrir");
    }
}
$("#abrir").click(function() {
    if ($("#abrir>h3").text() === "Fechar") {
        $('.carousel').carousel('next');
        setTimeout(checkConditions, 1500);
    }
    else {
        checkConditions();
    }
});
$("#foto").attr("src", "imgs/" + getUrlParameter("distrito") + ".jpg");