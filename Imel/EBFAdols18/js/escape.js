$(document).ready(function() {
    var app = new Vue({
        el: '#app',
        data: {
            n1: "",
            n2: "",
            solved: false
        },
        methods: {
            btnClick: function(n) {
                if (this.solved) return;
                if (this.n1.length == 0) {
                    this.n1 = n;
                }
                else if (this.n2.length == 0) {
                    this.n2 = n;
                    if (this.n1 === "4" && this.n2 === "7") {
                        this.solved = true;
                        var audio = new Audio('snd/Siren.mp3');
                        audio.play();
                        $('.numero').addClass("solved");
                        setInterval(function() {
                            var b = $('body');
                            var cls = 'blink';
                            if (b.hasClass(cls)) {
                                b.removeClass(cls);
                            }
                            else {
                                b.addClass(cls);
                            }
                        }, 500);
                    }
                    else {
                        var audio = new Audio('snd/Wrong.mp3');
                        audio.play();
                        $('.numero').effect( "shake" );
                    }
                }
                else {
                    this.n1 = n;
                    this.n2 = "";
                }
            }
        }
    });
});
