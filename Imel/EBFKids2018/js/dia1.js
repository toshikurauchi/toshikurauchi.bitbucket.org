$(document).ready(function() {
    var app = new Vue({
        el: '#app',
        data: {
            partes: [
                { texto: "\"Se você disser", img: 'img/megafone.png' },
                { texto: "com a sua boca:", img: 'img/boca.png' },
                { texto: "‘Jesus é Senhor’", img: 'img/cruz.png' },
                { texto: "e no seu coração", img: 'img/coracao.png' },
                { texto: "crer que Deus", img: 'img/deus.png' },
                { texto: "ressuscitou Jesus,", img: 'img/ressuscitou.png' },
                { texto: "você será salvo.\"", img: 'img/salvo.gif' },
                { texto: "Romanos 10.9", img: 'img/romano.png' },
            ]
        },
        methods: {
            thId(idx) {
                return "th" + idx;
            },
            tvId(idx) {
                return "tv" + idx;
            }
        }
    });
});

function btnclick(event) {
    el = event.srcElement;
    el = $(el).closest(".botao");
    var nxt = $("#" + $(el).attr("nextbtn"));
    $(el).removeClass("scale-in");
    $(el).addClass("scale-out");
    nxt.removeClass("scale-out");
    nxt.addClass("scale-in");
}
