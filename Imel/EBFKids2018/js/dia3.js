PALAVRAS = [
    ['"', 'V', 'ã', 'o'],
    ['p', 'e', 'l', 'o'],
    ['m', 'u', 'n', 'd', 'o'],
    ['i', 'n', 't', 'e', 'i', 'r', 'o'],
    ['e'],
    ['a', 'n', 'u', 'n', 'c', 'i', 'e', 'm'],
    ['o'],
    ['e', 'v', 'a', 'n', 'g', 'e', 'l', 'h', 'o'],
    ['a'],
    ['t', 'o', 'd', 'a', 's'],
    ['a', 's'],
    ['p', 'e', 's', 's', 'o', 'a', 's', '.”']
]

$(function() {
    app = new Vue({
        el: '#app',
        data: {
            palavras: embaralhaPalavras()
        },
        watch: {
            palavras: init
        }
    });
    init();
});

function init() {
    $(".sortable").each(function(idx, el) {
        $(el).sortable();
        $(el).disableSelection();
    });
    $("#solve").click(solve);
}

function embaralhaPalavras() {
    var i;
    var palavras = [];
    for (i = 0; i < PALAVRAS.length; i++) {
        palavras.push(shuffleArray(PALAVRAS[i].slice()));
    }
    return palavras;
}

function shuffleArray(o){
    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

function solve() {
    app.palavras = PALAVRAS.slice();
}
